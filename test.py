'''
	test configuration.
	
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils
from collections import Counter
from prettytable import PrettyTable
from pylab import plt
from pylib.simplemysql import SimpleMysql

if __name__ == '__main__':
    print "You're expected to see printout of SimpleMysql and Tweepy API instances"
    print "if you do not see error messages, then you're ready to go"
    print 
 
    
    # test twitter configuration
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.oauth_token, config.oauth_token_secret)
    
    api = tweepy.API(auth)
    tweets = api.user_timeline(id="CarlsonNews")
   